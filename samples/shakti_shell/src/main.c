/*
 * Copyright (c) 2012-2014 Wind River Systems, Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 */	
#include <zephyr.h>
#include <shell/legacy_shell.h>
 
static int shell_cmd_ping(int argc, char *argv[])
{
 ARG_UNUSED(argc);
 ARG_UNUSED(argv);
 
 printk("pong\n");
 
 return 0;
}
 
static int shell_cmd_params(int argc, char *argv[])
{
 int cnt;
 
 printk("argc = %d\n", argc);
 for (cnt = 0; cnt < argc; cnt++) {
 printk("  argv[%d] = %s\n", cnt, argv[cnt]);
 }
 return 0;
}
 
#define MY_SHELL_MODULE "test"
 
static struct shell_cmd commands[] = {
 { "ping", shell_cmd_ping, NULL },
 { "params", shell_cmd_params, "print argc" },
 { NULL, NULL, NULL }
};
 
 
static void myshell_init(void)
{
 SHELL_REGISTER(MY_SHELL_MODULE, commands);
}
 
 
 
void main(void)
{
myshell_init();
 while (1) {
 k_sleep(2000);
 }
}
